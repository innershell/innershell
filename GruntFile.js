module.exports = function(grunt) {
    grunt.initConfig({
        concat: {
            options: {
                separator: '\n/*----------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/\n\n'
            },
            vendorJs: {
                src: [
                    'app/public/js/lib/angular.min.js',
                    'app/public/js/lib/angular-ui-router.min.js',
                    'app/public/js/lib/jquery-1.11.3.min.js',
                    'app/public/js/lib/bootstrap.min.js'
                ],
                dest: 'app/public/js/lib/vendor.js'
            },
            investorWalletJs: {
                src: [
                    'app/public/js/framework/validation.js',
                    'app/public/js/framework/capcha.js',
                    'app/public/js/framework/config.js',
                    'app/public/js/framework/services.js',
                    'app/public/js/framework/controllers.js',
                    'app/public/js/framework/directives.js',
                    'app/public/js/framework/filters.js',
                    'app/public/js/framework/app.js'
                ],
                dest: 'app/public/js/framework/investors_wallet.js'
            },
            css: {
                src: 'app/public/css/*.css',
                dest: 'app/public/css/investors_wallet.css'
              }
        },
        uglify: {
            build: {
                files: {
                    'app/public/js/lib/vendor.min.js': ['app/public/js/lib/vendor.js'],
                    'app/public/js/framework/investors_wallet.min.js': ['app/public/js/framework/investors_wallet.js']
                 }
            }
        } ,
        
        cssmin: {
            target: {
                files: [{
                    expand: true,
                    cwd: 'app/public/css',
                    src: ['investors_wallet.css'],
                    dest: 'app/public/css',
                    ext: '.min.css'
                }]
            }
        }
    });
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-uglify'); // load the given tasks
    grunt.loadNpmTasks('grunt-contrib-cssmin');
    grunt.registerTask('default', ['concat','uglify',"cssmin"]); // Default grunt tasks maps to grunt
};