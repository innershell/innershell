/**
 * Created with JetBrains WebStorm.
 * User: root
 * Date: 2/10/15
 * Time: 8:21 PM
 * To change this template use File | Settings | File Templates.
 */

angular.module("iw-services", [])
    .factory('ajaxInterceptor', ['$q', '$location', "$rootScope", function ($q, $location, $rootScope) {
        return {
            request: function (config) {
                $rootScope._loading = true;
                config.headers = config.headers || {};
                config.headers['ajax'] = 1;
                return config;
            },
            'requestError': function (rejection) {
                $rootScope._loading = false;
                return $q.reject(rejection);
            },
            response: function (response) {
                $rootScope._loading = false;
                return response;
            },
            'responseError': function (rejection) {
                $rootScope._loading = false;
                return $q.reject(rejection);
            }
        };
    }])
    .factory("adapter", ["$http", "$q", "$rootScope", function ($http, $q, $rootScope) {
        var Adapter = function () {
            var self = this;
            self.call = function (request) {
                if (!request.method) {
                    return false;
                }
                var deferred = $q.defer();
                switch (request.method.toLowerCase()) {
                    case 'get' :
                        if (!request.query) {
                            request.query = {};
                        }
                        if (request.query.q) {
                            request.query.q = JSON.stringify(request.query.q);
                        }
                        if (request.query.filters) {
                            request.query.filters = JSON.stringify(request.query.filters);
                        }
                        $http.get('/collection/' + request.collectionName + '/_/' + request.command, {params: request.query}).then(function (response) {
                            console.log("==============data======================", response.data)
                            deferred.resolve(response.data);
                        }, function (error) {
                            $rootScope.iw.broadcast(error.data)
                            deferred.reject(error.data);
                        })
                        return deferred.promise;

                        break;
                    case  'post' :
                        $http.post('/collection/' + request.collectionName + '/_/' + request.command, request.data).then(function (response) {
                            console.log("==============data======================", response.data)
                            deferred.resolve(response.data);
                        }, function (error) {
                            $rootScope.iw.broadcast(error.data)
                            deferred.reject(error.data);
                        })
                        return deferred.promise;

                        break;
                    case 'put':
                        $http.put('/collection/' + request.collectionName + '/_/' + request.command, request.data, {params: request.query}).then(function (response) {
                            console.log("==============data======================", response.data)
                            deferred.resolve(response.data);
                        }, function (error) {
                            $rootScope.iw.broadcast(error.data.data)
                            deferred.reject(error.data);
                        })
                        return deferred.promise;
                        break;
                    default :
                        console.log("Wrong custom type");
                }
            };

            self.list = function (request) {
                if (!request.query) {
                    request.query = {};
                }
                var deferred = $q.defer();
                if (request.query.q) {
                    request.query.q = JSON.stringify(request.query.q);
                }
                $http.get('/collection/' + request.collectionName, {params: request.query}).then(function (response) {
                    console.log("==============data======================", response.data)
                    deferred.resolve(response.data);
                }, function (error) {
                    deferred.reject(error.data);
                })
                return deferred.promise;
            };

            self.get = function (request) {
                var deferred = $q.defer();
                $http.get('/collection/' + request.collectionName + "/" + request.collectionId).then(function (response) {
                    console.log("==============data======================", response.data)
                    deferred.resolve(response.data);
                }, function (error) {
                    deferred.reject(error.data);
                })
                return deferred.promise;
            };

            self.add = function (request) {
                var deferred = $q.defer();
                $http.post('/collection/' + request.collectionName, request.data).then(function (response) {
                    console.log("==============data======================", response.data)
                    deferred.resolve(response.data);
                }, function (error) {
                    deferred.reject(error.data);
                })
                return deferred.promise;
            };

            self.update = function (request) {
                var deferred = $q.defer();
                $http.put('/collection/' + request.collectionName + "/" + request.collectionId, request.data).then(function (response) {
                    console.log("==============data======================", response.data)
                    deferred.resolve(response.data);
                }, function (error) {
                    deferred.reject(error.data);
                })
                return deferred.promise;
            };

            self.delete = function (request) {
                var deferred = $q.defer();
                $http['delete']('/collection/' + request.collectionName + "/" + request.collectionId).then(function (response) {
                    console.log("==============data======================", response.data)
                    deferred.resolve(response.data);
                }, function (error) {
                    deferred.reject(error.data);
                })
                return deferred.promise;
            };
        }
        return new Adapter();
    }])

    .service('$upload', ['$http', '$timeout', function ($http, $timeout) {
        function sendHttp(config) {
            if (window.XMLHttpRequest) {
                if (window.FormData) {
                    // allow access to Angular XHR private field: https://github.com/angular/angular.js/issues/1934
                    XMLHttpRequest = (function (origXHR) {
                        return function () {
                            var xhr = new origXHR();
                            xhr.setRequestHeader = (function (orig) {
                                return function (header, value) {
                                    if (header === '__setXHR_') {
                                        var val = value(xhr);
                                        // fix for angular < 1.2.0
                                        if (val instanceof Function) {
                                            val(xhr);
                                        }
                                    } else {
                                        orig.apply(xhr, arguments);
                                    }
                                }
                            })(xhr.setRequestHeader);
                            return xhr;
                        }
                    })(XMLHttpRequest);
                    window.XMLHttpRequest.__isShim = true;
                }
            }

            config.method = 'POST';
            config.headers = config.headers || {};
            config.transformRequest = config.transformRequest || function (data, headersGetter) {
                if (window.ArrayBuffer && data instanceof window.ArrayBuffer) {
                    return data;
                }
                return $http.defaults.transformRequest[0](data, headersGetter);
            };

            if (window.XMLHttpRequest.__isShim) {
                config.headers['__setXHR_'] = function () {
                    return function (xhr) {
                        if (!xhr) return;
                        config.__XHR = xhr;
                        config.xhrFn && config.xhrFn(xhr);
                        xhr.upload.addEventListener('progress', function (e) {
                            if (config.progress) {
                                $timeout(function () {
                                    if (config.progress) config.progress(e);
                                });
                            }
                        }, false);
                        //fix for firefox not firing upload progress end, also IE8-9
                        xhr.upload.addEventListener('load', function (e) {
                            if (e.lengthComputable) {
                                if (config.progress) config.progress(e);
                            }
                        }, false);
                    };
                };
            }

            var promise = $http(config);

            promise.progress = function (fn) {
                config.progress = fn;
                return promise;
            };

            promise.abort = function () {
                if (config.__XHR) {
                    $timeout(function () {
                        config.__XHR.abort();
                    });
                }
                return promise;
            };
            promise.xhr = function (fn) {
                config.xhrFn = fn;
                return promise;
            };
            promise.then = (function (promise, origThen) {
                return function (s, e, p) {
                    config.progress = p || config.progress;
                    var result = origThen.apply(promise, [s, e, p]);
                    result.abort = promise.abort;
                    result.progress = promise.progress;
                    result.xhr = promise.xhr;
                    result.then = promise.then;
                    return result;
                };
            })(promise, promise.then);

            return promise;
        }

        this.upload = function (config) {
            config.headers = config.headers || {};
            config.headers['Content-Type'] = undefined;
            config.transformRequest = config.transformRequest || $http.defaults.transformRequest;
            var formData = new FormData();
            var origTransformRequest = config.transformRequest;
            var origData = config.data;
            config.transformRequest = function (formData, headerGetter) {
                if (origData) {
                    if (config.formDataAppender) {
                        for (var key in origData) {
                            var val = origData[key];
                            config.formDataAppender(formData, key, val);
                        }
                    } else {
                        for (var key in origData) {
                            var val = origData[key];
                            if (typeof origTransformRequest == 'function') {
                                val = origTransformRequest(val, headerGetter);
                            } else {
                                for (var i = 0; i < origTransformRequest.length; i++) {
                                    var transformFn = origTransformRequest[i];
                                    if (typeof transformFn == 'function') {
                                        val = transformFn(val, headerGetter);
                                    }
                                }
                            }
                            formData.append(key, val);
                        }
                    }
                }

                if (config.file != null) {
                    var fileFormName = config.fileFormDataName || 'file';

                    if (Object.prototype.toString.call(config.file) === '[object Array]') {
                        var isFileFormNameString = Object.prototype.toString.call(fileFormName) === '[object String]';
                        for (var i = 0; i < config.file.length; i++) {
                            formData.append(isFileFormNameString ? fileFormName + i : fileFormName[i], config.file[i], config.file[i].name);
                        }
                    } else {
                        formData.append(fileFormName, config.file, config.file.name);
                    }
                }
                return formData;
            };

            config.data = formData;

            return sendHttp(config);
        };

        this.http = function (config) {
            return sendHttp(config);
        }
    }])

    .factory('userProfile', ["$rootScope", "adapter", function ($rootScope, adapter) {
        return {
            get: function (cb) {
                var request = {
                    command: "getUserProfile",
                    collectionName: "iw.users",
                    query: {
                        q: {_id: $rootScope.iw.user._id }
                    },
                    method: 'get'
                }
                adapter.call(request).then(function (response) {
                    cb(null, response.data);
                }, function (response) {
                    cb(response.data, null);
                });
            },
            getPaymentDetails: function (cb) {
                if ($rootScope.iw.user.role != "borrower") {
                    return cb("invalid user role", null);
                }
                var request = {
                    command: "getBorrowerPaymentDetails",
                    collectionName: "iw.payments",
                    query: {
                        q: {_id: $rootScope.iw.user._id }
                    },
                    method: 'get'
                }
                adapter.call(request).then(function (response) {
                    cb(null, response.data);
                }, function (response) {
                    cb(response.data, null);
                });

            }
        }
    }])

    .factory('modalTemplate', ["$rootScope", "$http", "$compile", "$timeout", "adapter", function ($rootScope, $http, $compile, $timeout, adapter) {
        var template = {},
            getTemplate = function (template_name, cb) {
                if (template[template_name]) {
                    return cb(template[template_name]);
                }
                $http({method: "GET", url: "/partials/modal/" + template_name + ".html"}).then(function (response) {
                    template[template_name] = response.data;
                    cb(template[template_name]);
                }, function (error) {
                    cb()
                })
            }
        return {
            appendTemplate: function (template_name, $scope) {
                getTemplate(template_name, function (template) {
                    if (!template) {
                        return console.log("=====Error in getting template for modal========", template_name)
                    }
                    $compile(template)($scope, function (elem, scope) {
                        $timeout(function () {
                            $(elem).appendTo("body").modal("show");
                        })
                    });
                    if (!$scope.$$phase && !$scope.$root.$$phase) {
                        $scope.$apply();
                    }
                })
            },
            removeModal: function () {
                if ($(".modal.in").length) {
                    $(".modal.in").modal('hide');
                }
            }
        }
    }])

    .factory('messageSvc', ["$rootScope", "$http", "$compile", "$timeout", "adapter", function ($rootScope, $http, $compile, $timeout, adapter) {
        return {
            "000500": {
                message: "There is some technical issue. You can try after sometime.",
                level: "error"
            },
            "000501": {
                message: "Something unexpected happened. You can try after sometime",
                level: "error"
            },
            "000100": {
                message: " You are already registered with this Email ID",
                level: "warning"
            },
            "000101": {
                message: " You have provided wrong city. Please select one of the allowed city",
                level: "warning"
            },
            "000102": {
                message: " Email Id or Password is wrong",
                level: "warning"
            },
            "000103": {
                message: " Your account is not yet verified",
                level: "warning"
            },
            "000104": {
                message: " You are not registered for given role. Please Check",
                level: "error"
            },
            "000105": {
                message: " Invalid Verification code. Please Check",
                level: "warning"
            }
        }
    }])


