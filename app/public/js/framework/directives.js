/**
 * Created with JetBrains WebStorm.
 * User: root
 * Date: 2/10/15
 * Time: 8:18 PM
 * To change this template use File | Settings | File Templates.
 */
angular.module("iw-directives", [])
    .directive('image', [function () {
        var filler = $('<div class="filler"></div>'),
            loading = '<div> Loading...</div>';

        return {
            restrict: 'A',
            scope: {},

            link: function (scope, ele, attrs) {


                if (!attrs.aspectratio) {
                    return;
                }
                scope.src = attrs.image;
                $(ele).addClass('ms-img-warpper')
                var height = 0, width = 0,
                    valign = (attrs.valign) ? attrs.valign : 'bottom',
                    halign = (attrs.halign) ? attrs.halign : 'center',
                    img = $(ele).children()[0],
                    placeholderHtml = (attrs.placeholderhtml) ? attrs.placeholderhtml : '<span></span>',
                    aspect_ratio = attrs.aspectratio.split(':'),
                    initCss = function () {
                        var padding = parseInt((aspect_ratio[1] / aspect_ratio[0]) * 100);
                        if (attrs.centercropping == true || attrs.centercropping == 'true') {
                            $(img).css('left', '-100%');
                            $(img).css('right', '-100%');
                            $(img).css('top', '-100%');
                            $(img).css('bottom', '-100%');
                            $(img).css('position', 'absolute');
                            $(img).css('margin', 'auto');
                            $(img).css('height', 'auto');
                            $(img).css('width', 'auto');
                        }
                        else {
                            console.log("=========2=========", img)

                            $(img).addClass('ms-img-content');
                            switch (valign) {
                                case 'top':
                                    $(img).css('top', '0');

                                    break;
                                case 'bottom':
                                    $(img).css('bottom', '0');
                                    break;
                                case 'middle':
                                    $(img).css('top', '0');
                                    $(img).css('bottom', '0');
                                    $(img).css('margin-top', 'auto');
                                    $(img).css('margin-bottom', 'auto');
                                    break;
                            }
                            switch (halign) {
                                case 'left':
                                    $(img).css('left', '0');
                                    break;
                                case 'center':
                                    $(img).css('left', '0');
                                    $(img).css('right', '0');
                                    $(img).css('margin-left', 'auto');
                                    $(img).css('margin-right', 'auto');
                                    break;
                                case 'right':
                                    $(img).css('right', '0');
                                    break;
                            }
                        }
                        if (attrs.loadinghtml) {
                            loading = attrs.loadinghtml;
                        }
                        var container = '<div style="position: absolute;width: 100%;height: 100%;"><table style="width: 100%; height: 100%;"><tbody><tr><td style=" text-align: center">' + loading + '</td></tr></tbody></table></div> ';
                        /*loading = $(container);
                         $(loading).insertBefore(img);*/
                        //$(filler).insertAfter(img);
                        //$(ele).append(filler);
                        scope.padding = padding + '%'
                        // $(ele).find('img').next().css('padding-top', padding + '%');
                        //$(img).hide();
                    };
                aspect_ratio[0] = parseInt(aspect_ratio[0]);
                aspect_ratio[1] = (aspect_ratio[1]) ? parseInt(aspect_ratio[1]) : 1;


                if (attrs.width && attrs.height) {
                    width = parseInt(attrs.width);
                    height = parseInt(attrs.height);
                    var ab = function (a, b) {
                        return (b == 0) ? a : ab(b, a % b);
                    }
                    var gcd = ab(width, height);
                    aspect_ratio[0] = width / gcd;
                    aspect_ratio[1] = height / gcd;
                    initCss();
                }
                else if (!attrs.width && attrs.height) {
                    width = parseInt((parseInt(attrs.height * aspect_ratio[0]) / aspect_ratio[1]));
                    height = parseInt(attrs.height);
                    attrs.width = width;
                    initCss();
                }
                else if (attrs.width && !attrs.height) {
                    height = parseInt((parseInt(attrs.width * aspect_ratio[1]) / aspect_ratio[0]));
                    width = parseInt(attrs.width);
                    attrs.height = height;
                    initCss();
                }
                else {
                    initCss();
                }
                $(img).load(function (e) {
                    $(img).show();
                    if ($(img).prev()) {
                        $(img).prev().remove();
                    }
                });
                $(img).error(function () {
                    if ($(img).prev()) {
                        $(img).prev().remove();
                    }
                    $(img).hide();
                    if (placeholderHtml) {
                        $(placeholderHtml).insertBefore(img);
                    }
                });
            },
            template: '<img ng-src="{{src}}"><div class="filler"  ng-style="{\'padding-top\':padding}"></div>'
        }
    }])
    .directive('upload', ['$upload' , function ($upload) {
        return {
            restrict: "A",
            require: 'ngModel',
            link: function (scope, ele, attrs, ctrl) {
                function handleFileChange(e) {
                    var ngModel = ctrl;
                    if (!ngModel) return;
                    if (!e.target.files || !e.target.files.length) return;
                    var file = e.target.files[0];
                    scope.uploading = true;
                    var data = {
                        versioning: false
                    }
                    scope.upload = $upload.upload({
                        url: '/ms/uploads',                        //  'http://uploads.storehippo.com:5010/ms/uploads',
                        data: data,
                        file: file
                    }).success(function (data, status, headers, config) {
                            data.uploadType = 'tmp';
                            scope.previewUrl = e.target.result;
                            ngModel.$setViewValue(data);
                            scope.uploading = false;
                            if (!scope.$$phase) {
                                scope.$apply();
                            }
                        }).error(function (data, status) {
                            //data and status are interchanging their values - Rohit
                            if (data) {
                                alert(data);
                            } else {
                                alert("Error occured in file uploading");
                            }
                            scope.uploading = false;
                        });
                }

                ele.bind("change", handleFileChange);
            }
        }

    }])
    .directive("slides", [ function () {
        return {
            restrict: "A",
            scope: true,
            link: {
                post: function (scope, ele, attrs) {
                    var id = attrs.id;
                    scope.next = function () {
                        $('#' + id).carousel('next');
                    }
                    scope.prev = function () {
                        $('#' + id).carousel('prev');
                    }
                    var interval = (attrs.interval) ? attrs.interval : 5000;
                    if (!!$.prototype.carousel) {
                        $('#' + id).carousel({interval: interval });
                    }
                }
            }
        }
    }])
    .directive("inputStyle", [function () {
        return {
            restrict: "A",
            link: function (scope, element, attrs, ngModel) {
                if (element.value && element.value.trim() !== '') {
                    $(element).parent().addClass('input--filled');
                }
                $(element).on('focus', onInputFocus);
                $(element).on('blur', onInputBlur);

                function onInputFocus(ev) {
                    $(element).parent().addClass('input--filled');
                }

                function onInputBlur(ev) {
                    if (ev.target.value.trim() === '') {
                        $(element).parent().removeClass('input--filled');
                    }
                }
            }
        }
    }])
    .directive("states", ["adapter", function (adapter) {
        var states = null;
        return{
            restrict: "A",
            link: function (scope, element, attrs) {
                if (states) {
                    return scope.states = states;
                }
                else {
                    var request = {
                        command: "getStates",
                        collectionName: "iw.geo-data",
                        method: 'get',
                        query: {
                            q: {}
                        }
                    }
                    adapter.call(request).then(function (response) {
                        if (response && response.states) {
                            states = response.states;
                            scope.states = states;
                        }
                    }, function (response) {
                        console.log('===================Error in getting cities from api===============', response)
                    });
                }
            }
        }
    }])
    .directive("cities", ["adapter", function (adapter) {
        var cities = {};
        return{
            restrict: "A",
            link: function (scope, element, attrs) {
                attrs.$observe("state", function (state) {
                    if (!state) {
                        state = "delhi";
                    }
                    if (cities[state]) {
                        return scope.cities = cities[state];
                    }
                    else {
                        var request = {
                            command: "getCities",
                            collectionName: "iw.geo-data",
                            method: 'get',
                            query: {
                                q: {
                                    state: state
                                }
                            }
                        }
                        adapter.call(request).then(function (response) {
                            if (response && response.cities) {
                                cities[state] = response.cities;
                                scope.cities = cities[state];
                            }
                        }, function (response) {
                            console.log('===================Error in getting cities from api===============', response)
                        });
                    }
                })
            }
        }
    }])
    .directive("multiselect", ["$rootScope", "$timeout", function ($rootScope, $timeout) {
        return {
            require: "ngModel",
            link: function (scope, element, attrs, ngmodel) {
                if (!scope.fields[attrs.name]) {
                    scope.fields[attrs.name] = [];
                }
                $timeout(function () {
                    $(element).select2({
                        placeholder: attrs["placeholder"],
                        tokenSeparators: [","]
                    }).bind("change", function () {
                            ngmodel.$setViewValue($(element).select2('val'));
                            scope.$apply();
                        });
                }, 100)
                ngmodel.$parsers.unshift(function (values) {
                    if (!values || !values.length) {
                        return null;
                    }
                    var temp = [];
                    angular.forEach(values, function (value) {
                        temp.push(value.split(":").pop());
                    })
                    return temp;
                })
                ngmodel.$formatters.unshift(function (values) {
                    if (!values || !values.length) {
                        return null;
                    }
                    var temp = [];
                    angular.forEach(values, function (value) {
                        temp.push("string:" + value);
                    })
                    $(element).select2('val', temp)
                    return values;
                })
            }
        }
    }])
    .directive('showtab', [function () {
        return {
            link: function (scope, element, attrs) {
                element.on("click", function (e) {
                    e.preventDefault();
                    $(element).tab('show');
                });
            }
        };
    }])
    .directive('modal', ['$compile', '$timeout', "$http", function ($compile, $timeout, $http) {
        return {
            link: function (scope, element, attrs) {
                var template = null;
                var appendTemplate = function () {
                    $compile(template)(scope, function (elem, scope) {
                        $timeout(function () {
                            $(elem).appendTo("body").modal("show");

                        })
                    });
                    if (!scope.$$phase && !scope.$root.$$phase) {
                        scope.$apply();
                    }
                }
                element.bind('click', function () {
                    $(".modal").remove();
                    $(".modal-backdrop").remove();
                    if (template) {
                        appendTemplate();
                        return;
                    }
                    $http({method: "GET", url: "/partials/modal/" + attrs.template + ".html"}).then(function (response) {
                        template = response.data;
                        appendTemplate();
                    }, function (error) {

                    })
                });
            }
        }
    }])
    .directive('validateRange', [function () {
        return {
            restrict: "A",
            require: "ngModel",
            link: function (scope, element, attrs, ngModel) {
                ngModel.$parsers.unshift(function (value) {
                    if (!value || !scope.fields[attrs.fieldName]) {
                        return value;
                    }
                    if (attrs.validateRange == 'min') {
                        if (parseInt(value) > parseInt(scope.fields[attrs.fieldName])) {
                            scope.fields[attrs.fieldName] = "";
                        }
                    }
                    if (attrs.validateRange == 'max') {
                        if (parseInt(value) < parseInt(scope.fields[attrs.fieldName])) {
                            scope.fields[attrs.fieldName] = "";
                        }
                    }
                    return value;
                })
            }
        };
    }])
    .directive("progressBar", ["$timeout", function ($timeout) {
        return {
            restrict: "EA",
            scope: {
                total: '=total',
                complete: '=complete',
                barClass: '@barClass',
                completedClass: '=?'
            },
            transclude: true,
            link: function (scope, elem, attrs) {

                scope.label = attrs.label;
                scope.completeLabel = attrs.completeLabel;
                scope.showPercent = (attrs.showPercent) || false;
                scope.complete = scope.complete || 0;
                scope.completedClass = (scope.completedClass) || 'progress-bar-success';
                scope.$watch('complete', function () {
                    //change style at 100%
                    var progress = scope.complete;
                    if (progress >= 1) {
                        $(elem).find('.progress-bar').addClass(scope.completedClass);
                    }
                    else if (progress < 1) {
                        $(elem).find('.progress-bar').removeClass(scope.completedClass);
                    }

                });

            },
            template: "<div class='progress'>" +
                "<div class='progress-bar {{barClass}}' title='{{complete | number:2 }}%' style='width:{{complete}}%;'>" +
                "{{complete | number:2}}%" +
                "</div>" +
                "</div>"
        };
    }])
    .directive('loading', ['$http' , function ($http) {
        return {
            restrict: 'A',
            link: function (scope, elm, attrs) {
                scope.isLoading = function () {
                    return $http.pendingRequests.length > 0;
                };
                scope.$watch(scope.isLoading, function (v) {
                    if (v) {
                        $(elm).show();
                    } else {
                        $(elm).hide();
                    }
                });
            }
        };
    }])
    .directive("range", [function () {
        return {
            scope: true,
            restrict: "E",
            link: function (scope, ele, attrs) {

                scope.slider = {
                    floor: parseFloat(attrs.iwFloor),
                    ceil: parseFloat(attrs.iwCeil),
                    step: parseFloat(attrs.iwStep)
                }

            },
            template: ' <rzslider rz-slider-floor="slider.floor"' +
                'rz-slider-ceil="slider.ceil"' +
                'rz-slider-model="price.min"' +
                'rz-slider-high="price.max"' +
                'rz-slider-step="{{slider.step}}"' +
                'rz-slider-tpl-url="rzSliderTpl.html" rz-slider-hide-limit-labels="true"></rzslider>'
        }
    }])
    .directive("messages", ["$rootScope","$timeout","messageSvc", function ($rootScope,$timeout,messageSvc) {
        return {
            restrict: "A",
            scope: true,
            link: function (scope, elements, attrs) {
                var events = attrs.events.split(",");
                scope.events = [];
                scope.message="";
                scope.level="";
                if (!$rootScope.iw.registered_events) {
                    $rootScope.iw.registered_events = [];
                }
                angular.forEach(events, function (event) {
                    if ($rootScope.iw.registered_events.indexOf(event)<0) {
                        $rootScope.iw.registered_events.push(event);
                        scope.events.push(event);
                    }
                })
                scope.removeMessage=function(){
                    scope.message="";
                    scope.level="";
                }
                $rootScope.$on("iw.api_events", function (event,data) {
                    if(!data ||!data.code){
                        data={
                            code:"000500"
                        }
                    }
                    if ($rootScope.iw.registered_events.indexOf(data.code)<0 || scope.events.indexOf(data.code)< 0) {
                        return false;
                    }
                    var temp=messageSvc[data.code];
                    if(temp){
                        scope.message=temp.message;
                        scope.level=temp.level;
                        $timeout(function(){
                            scope.message="";
                            scope.level="";
                        },8000)
                    }
                    event.preventDefault();
                })
            }
        }
    }])
