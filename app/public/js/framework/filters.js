/**
 * Created with JetBrains WebStorm.
 * User: root
 * Date: 2/10/15
 * Time: 8:22 PM
 * To change this template use File | Settings | File Templates.
 */

angular.module("iw-filters",[])
    .filter("image", [function () {
        return function (value, attrs) {
            var base_url = "http://res.cloudinary.com/smartnomics/image/upload";
            return  base_url + value;
        }
    }])
    .filter("normalize", [function () {
        return function (input, all) {
            var reg = (all) ? /([^\W_]+[^\s-]*) */g : /([^\W_]+[^\s-]*)/;
            return (!!input) ? input.replace(reg, function(txt){return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();}) : '';
        }
    }])
    .filter('chunk', function () {
    function chunkArray(array, chunkSize) {
        var result = [];
        var currentChunk = [];

        for (var i = 0; i < array.length; i++) {
            currentChunk.push(array[i]);
            if (currentChunk.length == chunkSize) {
                result.push(currentChunk);
                currentChunk = [];
            }
        }

        if (currentChunk.length > 0) {
            result.push(currentChunk);
        }

        return result;
    }

    function defineHashKeys(array) {
        for (var i = 0; i < array.length; i++) {
            array[i].$$hashKey = i;
            //console.log(array[i]);
        }
    }

    return function (array, chunkSize) {
        if (!(array instanceof Array)) return array;
        if (!chunkSize) return array;

        var myApp = angular.module('iw-filters');
        // Create empty cache if it doesn't already exist.
        if (typeof myApp.chunkCache === "undefined") {
            myApp.chunkCache = [];
        }
        // Search the cache to see if we filtered the given array before.
        for (var i = 0; i < myApp.chunkCache.length; i++) {
            var cache = myApp.chunkCache[i];
            if (cache.array == array && cache.chunkSize == chunkSize) {
                return cache.result;
            }
        }
        // If not, construct the chunked result.
        var result = chunkArray(array, chunkSize);
        // And then add that result to the cache.
        var cache = {
            array: array,
            chunkSize: chunkSize,
            result: result
        };
        myApp.chunkCache.push(cache);
        return result;
    }
}) 