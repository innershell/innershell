/**
 * Created with JetBrains WebStorm.
 * User: root
 * Date: 29/8/15
 * Time: 9:03 AM
 * To change this template use File | Settings | File Templates.
 */

function calculateEMI(amount,time,rate) {
    if (isNaN(amount) || isNaN(rate) || isNaN(time)) {
        return null;
    }
    rate = parseFloat(rate) / 1200
    return parseInt(amount) * parseFloat(rate) / (1 - (Math.pow(1 / (1 + parseFloat(rate)), parseInt(time))));
}

function post_to_url(path, params, method) {
    method = method || "post"; // Set method to post by default if not specified.

    // The rest of this code assumes you are not using a library.
    // It can be made less wordy if you use one.
    var form = document.createElement("form");
    form.setAttribute("method", method);
    form.setAttribute("action", path);

    for (var key in params) {
        if (params.hasOwnProperty(key)) {
            var value = params[key];
            if (typeof params[key] == 'object') {
                value = JSON.stringify(value);
            }
            var hiddenField = document.createElement("input");
            hiddenField.setAttribute("type", "hidden");
            hiddenField.setAttribute("name", key);
            hiddenField.setAttribute("value", value);
            form.appendChild(hiddenField);
        }
    }
    document.body.appendChild(form);
    form.submit();
}

function load_files(filename, filetype){
    if (filetype=="js"){ //if filename is a external JavaScript file
        var fileref=document.createElement('script')
        fileref.setAttribute("type","text/javascript")
        fileref.setAttribute("src", filename)
    }
    else if (filetype=="css"){ //if filename is an external CSS file
        var fileref=document.createElement("link")
        fileref.setAttribute("rel", "stylesheet")
        fileref.setAttribute("type", "text/css")
        fileref.setAttribute("href", filename)
    }
    if (typeof fileref!="undefined")
        document.getElementsByTagName("head")[0].appendChild(fileref)
}


var User = null;

angular.module("investor", ['ui.router', 'sm-validation',"vcRecaptcha","rzModule","iw-config","iw-services","iw-directives","iw-controllers","iw-filters"])
    .run(["$rootScope", "$state", function ($rootScope, $state) {
        $rootScope.iw = {};
        $rootScope.iw.broadcast = function (event) {
            $rootScope.$broadcast("iw.api_events",event)
        }

        if (!String.prototype.trim) {
            (function () {
                // Make sure we trim BOM and NBSP
                var rtrim = /^[\s\uFEFF\xA0]+|[\s\uFEFF\xA0]+$/g;
                String.prototype.trim = function () {
                    return this.replace(rtrim, '');
                };
            })();
        }
        $rootScope.$on('$stateChangeStart',
            function (event, toState, toParams, fromState, fromParams) {
                var data= toState.data;
                if (data && data.user_secured && !$rootScope.iw.user.isLoggedIn) {
                    $state.go("home");
                    event.preventDefault();
                    return false;
                }
                if(data && data.role && $rootScope.iw.user.role!=data.role){
                    $state.go("home");
                    event.preventDefault();
                    return false;
                }
            })

        $rootScope.$on('$stateChangeSuccess',
            function (event, toState, toParams, fromState, fromParams) {
                document.body.scrollTop = document.documentElement.scrollTop = 0;
            })

        function mobilecheck() {
            var check = false;
            (function (a) {
                if (/(android|ipad|playbook|silk|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i.test(a) || /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0, 4)))check = true
            })(navigator.userAgent || navigator.vendor || window.opera);
            return check;
        }

        function hasParentClass(e, classname) {
            if (e === document) return false;
            if ($(e).hasClass(classname)) {
                return true;
            }
            return e.parentNode && hasParentClass(e.parentNode, classname);
        }

        var eventtype = mobilecheck() ? 'touchstart' : 'click',
            resetMenu = function () {
                $("#st-container").removeClass('st-menu-open');
            },
            bodyClickFn = function (evt) {
                if (!hasParentClass(evt.target, 'st-menu')) {
                    resetMenu();
                    $(document).unbind(eventtype, bodyClickFn);
                }
            }

        $("#st-trigger-effects").on(eventtype, function (ev) {
            ev.stopPropagation();
            ev.preventDefault();
            $("#st-container").addClass("st-effect-1");
            setTimeout(function () {
                $("#st-container").addClass('st-menu-open');
            }, 25);
            $(document).on(eventtype, bodyClickFn);
        })
    }])

    .run(["$rootScope", "$state", "adapter", function ($rootScope, $state, adapter) {
        User = function (user) {
            var self = this;
            self._id = user._id;
            self.isLoggedIn = user.isLoggedIn;
            self.first_name = user.first_name;
            self.last_name = user.last_name;
            self.middle_name = user.middle_name;
            self.email = user.email;
            self.mobile = user.mobile;
            self.role = user.role;
            self.role_id = user.role_id;

            return self;
        }
        User.prototype.logout = function () {
            if (!this.isLoggedIn) {
                return false;
            }
            var request = {
                command: "logout",
                collectionName: "iw.users",
                method: 'put',
                data: {}
            }
            adapter.call(request).then(function (response) {
                $rootScope.iw.user = new User(response.data);
                $state.go("home");

            }, function (response) {
                console.log('===========Error in performing logout================')
            })
        }
        $rootScope.iw.user = new User(session);
        console.log('=====================',config)
        $rootScope.iw.config=config;
    }])
