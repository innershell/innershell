/**
 * Created with JetBrains WebStorm.
 * User: root
 * Date: 11/10/15
 * Time: 3:48 PM
 * To change this template use File | Settings | File Templates.
 */

angular.module("iw-config",[])

.config(["$stateProvider", "$urlRouterProvider", "$locationProvider", "$httpProvider",
        function ($stateProvider, $urlRouterProvider, $locationProvider, $httpProvider) {
    $httpProvider.interceptors.push('ajaxInterceptor');
    $locationProvider.html5Mode({
        enabled: true,
        requireBase: false
    }).hashPrefix('!');
    $urlRouterProvider.otherwise("/");
    $stateProvider
        .state('home', {
            url: "/",
            templateUrl: "partials/home.html"
        })
        .state('about', {
            url: "/abou-t",
            templateUrl: "partials/about.html"
        })
        .state('contact', {
            url: "/contact-us",
            templateUrl: "partials/contact.html"
        })
        .state('login', {
            url: "/login",
            templateUrl: "partials/login.html"
        })
        .state('how-it-works', {
            url: "/how-it-works",
            templateUrl: "partials/how-it-works.html"
        })
        .state('why-trust-us', {
            url: "/why-trust-us",
            templateUrl: "partials/why-trust-us.html"
        })
        .state('faq', {
            url: "/faq",
            templateUrl: "partials/faq.html"
        })
        .state('privacy-policies', {
            url: "/privacy-policies",
            templateUrl: "partials/privacy-policy.html"
        })
        .state('terms-and-conditions', {
            url: "/terms-and-conditions",
            templateUrl: "partials/terms-and-conditions.html"
        })
        .state('code-verification', {
            url: "/verification/:email/:role",
            templateUrl: "partials/verification.html",
            controller: "VerificationCtrl"
        })

        /*=============For Borrowing UI==================*/
        .state('borrowing', {
            url: "/borrowing",
            templateUrl: "partials/borrowing.html ",
            abstract:true

        })
        .state('how-to-borrow', {
            url: "/how-to-borrow",
            templateUrl: "partials/borrowing/how-to-borrow.html",
            parent:"borrowing"
        })
        .state('responsible-borrowing', {
            url: "/responsible-borrowing",
            templateUrl: "partials/borrowing/responsible-borrowing.html ",
            parent:"borrowing"
        })
        .state('interest-rate-fee', {
            url: "/interest-rate-fee",
            templateUrl: "partials/borrowing/interest-rate-and-fee.html ",
            parent:"borrowing"
        })
        .state('create-effective-listing', {
            url: "/create-effective-listing",
            templateUrl: "partials/borrowing/create-effective-listing.html",
            parent:"borrowing"
        })
        .state('borrower_register', {
            url: "/borrowing/register",
            templateUrl: "partials/register_borrower.html",
            controller: "BorrowerRegisterCtrl"
        })

        /*=============For lending UI==================*/

        .state('lending', {
            url: "/lending",
            templateUrl: "partials/lending.html " ,
            abstract:true
        })
        .state('how-to-lend', {
            url: "/how-to-lend",
            templateUrl: "partials/lending/how-to-lend.html",
            parent:"lending"
        })
        .state('responsible-lending', {
            url: "/responsible-lending",
            templateUrl: "partials/lending/responsible-lending.html ",
            parent:"lending"
        })
        .state('interest-rates', {
            url: "/interest-rates",
            templateUrl: "partials/lending/interest-rates.html ",
            parent:"lending"
        })
        .state('risk-management', {
            url: "/risk-management",
            templateUrl: "partials/lending/risk-management.html ",
            parent:"lending"
        })
        .state('lender_register', {
            url: "/lending/register",
            templateUrl: "partials/register_lender.html",
            controller: "LenderRegisterCtrl"
        })

        /*=============For Profile Updation and View==================*/

        .state('profile', {
            url: '/profile',
            templateUrl: "partials/profile/profile.html",
            controller:"ProfileCtrl",
            abstract:true
        })
        .state('view', {
            url: '/view',
            templateUrl: "partials/profile/profile_view.html",
            controller:"ProfileViewCtrl",
            parent: 'profile'
        })
        .state('personal_details', {
            url: '/personal-details',
            templateUrl: "partials/profile/personal_details.html",
            controller:"PersonalDetailsCtrl",
            parent: 'profile'
        })
        .state('professional_details', {
            url: '/professional-details',
            templateUrl: "partials/profile/professional_details.html",
            controller: "ProfessionalDetailsCtrl",
            parent: 'profile'
        })
        .state('financial_details', {
            url: '/financial-details',
            templateUrl: "partials/profile/financial_details.html",
            controller: "FinancialDetailsCtrl",
            parent: 'profile'
        })
        .state('references', {
            url: '/references',
            templateUrl: "partials/profile/reference.html",
            controller: "ReferencesCtrl",
            parent: 'profile'
        })
        .state('payment', {
            url: '/payment',
            templateUrl: "partials/profile/payment.html",
            controller: "PaymentCtrl",
            parent: 'profile'
        })

        /*===============For Account====================*/

        .state('account', {
            url: '/account',
            templateUrl: "partials/account/account.html",
            data: {
                user_secured: "1"
            }
        })
        .state('dashboard', {
            url: '/dashboard',
            templateUrl: "partials/account/dashboard.html",
            parent: 'account'
        })
        .state('self_requests_list', {
            url: '/loan-requests-list',
            templateUrl: "partials/account/self_requests_list.html",
            controller: "SelfLoanRequestsListCtrl",
            parent: 'account'
        })
        .state('add_loan_request', {
            url: '/loan-requests/add',
            templateUrl: "partials/account/loan_request_form.html",
            controller: "LoanRequestsAddCtrl",
            parent: 'account'
        })
        .state('self_offer', {
            url: '/loan-offer',
            templateUrl: "partials/account/loan_offer_form.html",
            controller: "UserOfferCtrl",
            parent: 'account'
        })
        .state('bidding_requests', {
            url: '/bidding-requests',
            templateUrl: "partials/account/bidding_requests.html",
            controller: "BiddingRequestsCtrl",
            parent: 'account'
        })
        .state('bidding_history', {
            url: '/bidding-history',
            templateUrl: "partials/account/bidding_history.html",
            controller: "BiddingHistoryCtrl",
            parent: 'account'
        })


        .state('borrower_detail', {
            url: "/borrower/:_id",
            templateUrl: "partials/borrower_detail.html",
            controller: "BorrowerDetailCtrl",
            data:{
                user_secured:"1",
                role:"lender"
            }
        })
        .state('loan_request_detail', {
            url: '/loan-request/:_id',
            templateUrl: "partials/loan_request_detail.html",
            controller: "LoanRequestDetailCtrl" ,
            data:{
                user_secured:"1"
            }
        })

        /*================Listing Ctrl===================*/
        .state('loan_requests_list', {
            url: '/loan-requests/list',
            templateUrl: "partials/loan_requests_list.html",
            params: {
                city: null,
                min_interest:0,
                purpose:null
            },
            controller: "LoanRequestsListCtrl"
        })
        .state('loan_offers_list', {
            url: '/loan-offers/list',
            templateUrl: "partials/loan_offers_list.html",
            params: {
                city: null,
                max_interest:0,
                purpose:null
            },
            controller: "LoanOffersListCtrl"
        })

        }])

