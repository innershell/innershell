/**
 * Created with JetBrains WebStorm.
 * User: root
 * Date: 2/10/15
 * Time: 8:19 PM
 * To change this template use File | Settings | File Templates.
 */
angular.module("iw-controllers",[])
    .controller('MapCtrl', function ($scope) {
        var temp = function () {
            var mapOptions = {
                zoom: 10,
                center: new google.maps.LatLng(28.4345552, 77.0578855),
                mapTypeId: google.maps.MapTypeId.ROADMAP
            }

            var map = new google.maps.Map(document.getElementById('map'), mapOptions);
            var marker = new google.maps.Marker({
                map: map,
                position: new google.maps.LatLng(28.4345552, 77.0578855),
                title: "Smartnomics"
            });
            marker.content = '<div class="infoWindowContent">Registered Office  </div>';
            var infoWindow = new google.maps.InfoWindow();
            google.maps.event.addListener(marker, 'click', function () {
                infoWindow.setContent('<h2>' + marker.title + '</h2>' + marker.content);
                infoWindow.open($scope.map, marker);
            });
        }
        google.maps.event.addDomListener(window, "load", temp);
        $scope.openInfoWindow = function (e, selectedMarker) {
            e.preventDefault();
            google.maps.event.trigger(selectedMarker, 'click');
        }

    })
    .controller('EMICalculator', ["$scope", "$rootScope", "$timeout", function ($scope, $rootScope, $timeout) {
        $scope.fields = {};
        $scope.calculateEMI=function(){
            $scope.fields.emi = 0;
            $scope.fields.total_interest=0;
            $scope.fields.total_amount=0;
            if (!$scope.fields.amount || !$scope.fields.duration || !$scope.fields.interest) {
                return false;
            }
            var emi = calculateEMI($scope.fields.amount, $scope.fields.duration, $scope.fields.interest);
            if (emi) {
                $scope.fields.emi = Math.round(emi * 100) / 100;
                $scope.fields.total_amount= Math.round((parseFloat($scope.fields.emi)*parseInt($scope.fields.duration))* 100) / 100;
                $scope.fields.total_interest=Math.round((parseFloat($scope.fields.total_amount)-parseFloat($scope.fields.amount))* 100) / 100;
            }
        }
    }])

    .controller('FeedSummaryCtrl', ["$scope",function ($scope) {
        $scope.laa = {
            figure: "3.6 Lacs",
            diff: "0.8",
            type: "+"
        }
        $scope.air = {
            figure: "14.13%",
            diff: "0.44",
            type: "+"
        }
        $scope.lrr = {
            figure: "36",
            diff: "1",
            type: "-"
        }
        $scope.ip = {
            figure: "22",
            diff: "0.45",
            type: "+"

        }
    }])
    .controller('homeSearchCtrl', ["$scope", "$rootScope", "$state", function ($scope, $rootScope, $state) {
        var type="requests";
        $scope.fields={
            city:"delhi",
            interest_rate:"15.00",
            purpose:"education"
        };

        $scope.changeRole=function(curr_type){
            if(type!==curr_type){
                type= curr_type;
            }
        }

        $scope.submit=function(){
            if(type=="requests"){
                $state.go("loan_requests_list",{
                    city: $scope.fields.city,
                    min_interest:$scope.fields.interest_rate,
                    purpose:$scope.fields.purpose
                })
            }
            else if(type=="offers"){
                $state.go("loan_offers_list",{
                    city: $scope.fields.city,
                    max_interest:$scope.fields.interest_rate,
                    purpose:$scope.fields.purpose
                })
            }
        }

    }])
    .controller('EnquiryCtrl', ["$scope", "$rootScope", "$timeout","adapter", function ($scope, $rootScope, $timeout,adapter) {
        $scope.fields = {};
        $scope.submit=function(){
            var request = {
                command: "receiveEnquiry",
                collectionName: "iw.enquiries",
                method: 'post',
                data: $scope.fields
            }
            adapter.call(request).then(function (response) {

                },
                function (response) {
                    console.log('===================Error in registration===============', response)
                });
        }
    }])

    .controller("BorrowerRegisterCtrl", ["$scope", "$stateParams", "adapter", "$location","$rootScope", function ($scope, $stateParams, adapter, $location,$rootScope) {
        $scope.fields = {
            correspondence_address: {}
        };

        $scope.borrower=true;

        $scope.key=$rootScope.iw.config.capcha_key;



        $scope.submit = function () {
            $scope.fields["role"] = "borrower";
            delete $scope.fields["agree"];
            delete $scope.fields["understand"];
            var request = {
                command: "register",
                collectionName: "iw.users",
                method: 'post',
                data: $scope.fields
            }
            adapter.call(request).then(function (response) {
                    $location.path('/verification/' + $scope.fields.email + "/borrower")
                },
                function (response) {
                    console.log('===================Error in registration===============', response)
                });
        }
    }])
    .controller("LenderRegisterCtrl", ["$scope", "$stateParams", "adapter", "$location","$rootScope", function ($scope, $stateParams, adapter, $location,$rootScope) {

        $scope.fields = {
            correspondence_address: {}
        };

        $scope.lender=true;

        $scope.key=$rootScope.iw.config.capcha_key;

        $scope.submit = function () {
            $scope.fields["role"] = "lender";
            delete $scope.fields["agree"];
            delete $scope.fields["understand"];
            var request = {
                command: "register",
                collectionName: "iw.users",
                method: 'post',
                data: $scope.fields
            }
            adapter.call(request).then(function (response) {
                    $location.path('/verification/' + $scope.fields.email + "/lender")
                },
                function (response) {
                    console.log('===================Error in registration===============', response)
                });
        }
    }])
    .controller("VerificationCtrl", ["$scope", "$stateParams", "adapter", "$location", "$rootScope", function ($scope, $stateParams, adapter, $location, $rootScope) {
        $scope.fields = {
            code: ''
        }
        $scope.submit = function () {
            if (!$scope.fields.code || $scope.fields.code == "") {
                return $scope.error = "Please Enter Verification code";
            }
            var request = {
                command: "accountVerification",
                collectionName: "iw.users",
                method: 'post',
                data: {
                    email: $stateParams.email,
                    role: $stateParams.role,
                    code: $scope.fields.code
                }
            }
            adapter.call(request).
                then(function (response) {
                    $rootScope.iw.user = User(response.data);
                    $location.path('/profile/personal-details');
                }, function (response) {
                    console.log('===================Error in verification===============', response)
                });
        }
    }])
    .controller("LoginCtrl", ["$scope", "$location", "$rootScope", "adapter", function ($scope, $location, $rootScope, adapter) {
        $scope.errors = {};
        $scope.fields = {
            role: "borrower"
        };
        $scope.submit = function () {
            console.log('====================?>>>>>>>>>>>>>',$scope.fields)

            /*var data = new FormData();
            data.append("username", "bepi.fontana@libero.it");
            data.append("password", "11976");
            data.append("grant_type", "password");

            var xhr = new XMLHttpRequest();
            xhr.withCredentials = true;

            xhr.addEventListener("readystatechange", function () {
                if (this.readyState === this.DONE) {
                    console.log(this.responseText);
                }
            });

            //xhr.open("POST", "/testing");
            xhr.setRequestHeader("authorization", "Basic dGVjbmljaWFwcDpzZG9pZnMwc2FzZG9hag==");
            xhr.setRequestHeader("cache-control", "no-cache");
            xhr.setRequestHeader("postman-token", "d845118c-0da8-d304-c779-060bd19b7443");
*/
            //xhr.send(data);
            $scope.errors = {};
            if (!$scope.fields.email) {
                $scope.errors["email"] = "Email id is required";
            }
            else if (!/^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test($scope.fields.email)) {
                $scope.errors["email"] = "Enter valid email id is required";
            }
            if (!$scope.fields.password) {
                $scope.errors["password"] = "Password  is required";
            }
            if (Object.keys($scope.errors).length) {
                return  false
            }

            var request = {
                command: "login",
                collectionName: "iw.users",
                method: 'post',
                data: $scope.fields
            }
            adapter.call(request).then(function (response) {
                $rootScope.iw.user = new User(response.data);
                $location.path('/account/dashboard');
            }, function (error) {
                console.log("===========Error in login==============")
            })
        }
    }])

    /*=============Lender=================*/

    .controller('ProfileCtrl', ["$scope", "$rootScope", "$location","$state","adapter", function ($scope, $rootScope, $location,$state,adapter) {
        if(!$rootScope.iw.user.isLoggedIn){
            return $state.go("home");
        }
        $scope.ctrl={
            auth:true,
            active:1
        }
        if(!$state.is("personal_details")){
            $scope.ctrl.auth=false;
        }

        $scope.goto=function(state){
            $state.go(state);
            switch(state){
                case "professional_details":
                    $scope.ctrl.active=2;
                    $location.path("/profile/professional-details");
                    break;
                case "financial_details":
                    $scope.ctrl.active=3;
                    $location.path("/profile/financial-details");
                    break;
                case "references":
                    $location.path("/profile/references");
                    $scope.ctrl.active=4;
                    break;
                case "payment":
                    $scope.ctrl.active=5;
                    $location.path("/profile/payment");
                    break;
                default:
                    $location.path("/profile/personal-details");
                    $scope.ctrl.active=1;
            }
        }
    }])
    .controller('PersonalDetailsCtrl', ["$scope", "$rootScope", "$timeout","$state","adapter","userProfile",function ($scope, $rootScope, $timeout,$state,adapter,userProfile) {
        $scope.fields = {};

        if(!$scope.ctrl.auth) {
            $scope.ctrl.auth=true;
        }

        $scope.submit=function() {
            var request = {
                command: "updatePersonalDetails",
                collectionName: "iw.users",
                query:{
                    q:{ _id: $rootScope.iw.user._id}
                },
                method: 'put',
                data:$scope.fields
            }
            adapter.call(request).then(function (response) {
                    $state.go("professional_details");
                },
                function (response) {
                    console.log('===================Error in registration===============', response)
                });
        }

        userProfile.get(function(err,user){
            if(err){
                return $state.go("dashboard");
            }
            $scope.fields={
                first_name:user.first_name,
                middle_name:user.middle_name,
                last_name:user.last_name,
                gender:user.gender,
                email:user.email,
                mobile:user.mobile,
                marital_status:user.marital_status,
                correspondence_address:user.correspondence_address,
                permanent_address:user.permanent_address ,
                educational_qualification:user.educational_qualification ||{}
            };
        })
    }])
    .controller('ProfessionalDetailsCtrl', ["$scope", "$rootScope", "$timeout","$state","adapter","userProfile", function ($scope, $rootScope, $timeout,$state,adapter,userProfile) {
        if(!$scope.ctrl.auth){
            return $state.go("personal_details");
        }
        $scope.ctrl.active=2;
        $scope.fields = {
            employment_type:"salaried",
            office_state:"delhi"
        };
        $scope.submit=function(){
            var request = {
                command: "updateProfessionalDetails",
                collectionName: "iw.users",
                query:{
                    q:{
                        _id: $rootScope.iw.user._id
                    }
                },
                method: 'put',
                data: $scope.fields
            }
            adapter.call(request).then(function (response) {
                    $state.go("financial_details");
                },
                function (response) {
                    console.log('=================== Error in registration ===============', response)
                });
        }

        userProfile.get(function(err,user){
            if(err){
                return $state.go("dashboard");
            }
            $scope.fields=user.professional_details||{};
            if(!$scope.fields["employment_type"]) {
                $scope.fields["employment_type"]="salaried";
                $scope.fields["office_state"]="delhi";
            }
        })
    }])
    .controller('FinancialDetailsCtrl', ["$scope", "$rootScope", "$timeout","$state","adapter","userProfile", function ($scope, $rootScope, $timeout,$state,adapter,userProfile) {
        if(!$scope.ctrl.auth){
            return $state.go("personal_details");
        }

        $scope.ctrl.active=3;
        $scope.fields = {};

        $scope.submit=function(){
            var request = {
                command: "updateFinancialDetails",
                collectionName: "iw.users",
                query:{ q:{ _id: $rootScope.iw.user._id}},
                method: 'put',
                data: $scope.fields
            }
            adapter.call(request).then(function (response) {
                    if($rootScope.iw.user.role!="borrower"){
                         $state.go("dashboard");
                    }
                    else{
                        $state.go("references");
                    }
                },
                function (response) {
                    console.log('===================Error in updating FinancialDetails===============', response)
                });
        }

        userProfile.get(function(err,user){
            if(err){
                return $state.go("dashboard");
            }
            $scope.fields=user.financial_details||{};
        })
    }])
    .controller('ReferencesCtrl', ["$scope", "$rootScope", "$timeout","$state","adapter","userProfile", function ($scope, $rootScope, $timeout,$state,adapter,userProfile) {
        if(!$scope.ctrl.auth){
            return $state.go("personal_details");
        }
        if($rootScope.iw.user.role!="borrower"){
            return $state.go("dashboard");
        }
        $scope.ctrl.active=4;
        $scope.fields={
            referrer1:{},
            referrer2:{}
        }
        $scope.submit=function() {
            $state.go("payment");
            var request = {
                command: "updateReferences",
                collectionName: "iw.users",
                query:{
                    q:{_id: $rootScope.iw.user._id }
                },
                method: 'put',
                data: $scope.fields
            }
            adapter.call(request).then(function (response) {
                    $state.go("payment");
                },
                function (response) {
                    console.log('===================Error in registration===============', response)
                });
        }

        userProfile.get(function(err,user){
            if(err){
                return $state.go("dashboard");
            }
            if(user.references &&user.references.referrer1){
                $scope.fields=user.references;
            }
        })
    }])
    .controller('PaymentCtrl', ["$scope", "$rootScope", "$timeout","$state","adapter", "userProfile",function ($scope, $rootScope, $timeout,$state,adapter,userProfile) {
        if(!$scope.ctrl.auth){
            return $state.go("personal_details");
        }
        $scope.ctrl.active=5;
        $scope.show_payment_detail=false;
        $scope.data={
            check:0
        }
        $scope.move_to_account=function(skip){
            if(skip){
                var r=confirm("All the listings/loan requests will be provisionally accepted. Do you want to skip now?")
                if(r){
                    $state.go('account');
                }
            }
            else{
                $state.go('account');
            }
        }

        $scope.submit = function () {
            var request = {
                command: "payNow",
                collectionName: "iw.payments",
                query:{ q:{ _id: $rootScope.iw.user._id}},
                method: 'put',
                data: $scope.fields
            }
            adapter.call(request).then(function (response) {
                    if($rootScope.iw.user.role!="borrower"){
                        $state.go("dashboard");
                    }
                    var temp = response.data;
                    post_to_url(temp.url, temp.data, temp.action);
                },
                function (response) {
                    console.log('===================Error in creating payment request===============', response)
                });
        }

        userProfile.getPaymentDetails(function(err,result){
            if(result.details){
                $scope.show_payment_detail=true;
                $scope.payment_details=result.details;
            }
        })
    }])

    /*=================Account=======================*/
    /*=============Lender=================*/

    .controller('UserOfferCtrl', ["$scope", "$rootScope", "$timeout","$state","adapter",
        function ($scope, $rootScope, $timeout,$state,adapter) {
        $scope.fields={};
        $scope.submit=function() {
            var request = {
                command: "updateLoanOffer",
                collectionName: "iw.loan_offers",
                query:{
                    q:{
                        _id:$scope.fields._id
                    }
                },
                method: 'put',
                data: $scope.fields
            }
            adapter.call(request).then(function (response) {
                    $state.go("dashboard");
                },
                function (response) {
                    console.log('===================Error in registration===============', response)
                });
        }
            var request = {
                command: "getUserLoanOffer",
                collectionName: "iw.loan_offers",
                query:{
                    q:{_id: $rootScope.iw.user._id}
                },
                method: 'get'
            }
            adapter.call(request).then(function (response) {
                $scope.fields=response.data;
            },function (response) {
                console.log("=========Error in getting loan requests============")
            });

    }])
    .controller('BiddingRequestsCtrl', ["$scope", "$rootScope", "$timeout","$state","adapter","modalTemplate",
        function ($scope, $rootScope, $timeout,$state,adapter,modalTemplate) {
        var selected_bid=null;
        $scope.bidding_requests=[];
        $scope.amount=0;
        $scope.max_amount=0;
        $scope.errors={};

        $scope.sendBiddingRequest=function(){
            $scope.errors={};
            if(!$scope.amount) {
               return  $scope.errors["required"]=true;
            }
            if(isNaN($scope.amount)) {
                return  $scope.errors["numeric"]=true;
            }
            if(parseInt($scope.amount)>$scope.max_amount) {
                return  $scope.errors["max"]=true;
            }
            selected_bid["amount"]=$scope.amount
            var request = {
                command: "respondToBidRequest",
                collectionName: "iw.mappings",
                query:{
                    q:{_id: selected_bid._id}
                },
                data:selected_bid,
                method: 'put'
            }
            adapter.call(request).then(function (response) {
                $scope.errors={};
                $scope.amount=0;
                $scope.max_amount=0;
                selected_bid=null;
                $state.go($state.current, {}, {reload: true});
            },function (response) {
                $scope.errors={};
                selected_bid=null;
                $scope.amount=0;
                $scope.max_amount=0;
                console.log("=========Error in getting loan requests============")
            });
        }

        $scope.acceptBiddingRequest=function(req){
            selected_bid =req;
            var request = {
                command: "getMaxAmount",
                collectionName: "iw.mappings",
                query:{
                    q:{request_id: req.request_id,offer_id:req.offer_id}
                },
                method: 'get'
            }
            adapter.call(request).then(function (response) {
                $scope.amount=parseInt(response.amount);
                $scope.max_amount=$scope.amount;
                modalTemplate.appendTemplate("select_amount_for_mapping",$scope)

            },function (response) {
                selected_bid=null;
                console.log("=========Error in getting loan requests============")
            });
        }

        $scope.rejectBiddingRequest=function(req){
            var request = {
                command: "declineBidRequest",
                collectionName: "iw.mappings",
                query:{
                    q:{_id: req._id}
                },
                method: 'put'
            }
            adapter.call(request).then(function (response) {
                $state.go($state.current, {}, {reload: true});
            },function (response) {
                console.log("=========Error in getting loan requests============")
            });

        }

        var request = {
            command: "getLenderBiddingRequests",
            collectionName: "iw.mappings",
            query:{
                q:{lender_id: $rootScope.iw.user._id}
            },
            method: 'get'
        }
        adapter.call(request).then(function (response) {
            $scope.bidding_requests=response.data.records;
        },function (response) {
            console.log("=========Error in getting loan requests============")
        });
    }])
    .controller('BiddingHistoryCtrl', ["$scope", "$rootScope", "$timeout","$state","$compile","$http","$window","adapter",
        function ($scope, $rootScope, $timeout,$state,$compile,$http,$window,adapter) {
            if($rootScope.iw.user.role!="lender"/*||$rootScope.iw.user.status!="verified"*/){
                return $window.history.back();
            }
            var request={
                collectionName:"iw.mappings",
                command:"getLenderBids",
                query:{
                    q:{
                        lender_id:$rootScope.iw.user._id
                    }
                },
                method:"GET"
            }
            adapter.call(request).then(function(response){
                $scope.bids=response.data.records;

            },function(err){
                console.log("===Error in getting complete user details=====");
            })

        }])
    .controller('BorrowerDetailCtrl', ["$scope", "$rootScope", "$timeout","$state","$compile","$http","$window","adapter",
        function ($scope, $rootScope, $timeout,$state,$compile,$http,$window,adapter) {
            if(!(($rootScope.iw.user.role=="lender"&& $rootScope.iw.user.status=="verified") || $rootScope.iw.user._id==$state.params._id)){
                return $window.history.back();
            }
            var request={
                collectionName:"iw.users",
                command:"getBorrowerDetail",
                query:{
                    q:{
                        _id:$state.params._id
                    }
                },
                method:"GET"
            }
            adapter.call(request).then(function(response){
                $scope.user=response.data;

            },function(err){
                console.log("===Error in getting complete user details=====");
            })
     }])
    .controller('LoanRequestDetailCtrl', ["$scope", "$rootScope", "$timeout","$state","$compile","$http","$window","adapter",
        function ($scope, $rootScope, $timeout,$state,$compile,$http,$window,adapter) {
           /* if($rootScope.iw.user.status!="verified"){
                return $window.history.back();
            }*/
            $scope.loan_request={};
            var request={
                collectionName:"iw.loan_requests",
                command:"getLoanRequestDetail",
                query:{
                    q:{
                        _id:$state.params._id,
                        user_id:$rootScope.iw.user._id
                    }
                },
                method:"GET"
            }
            adapter.call(request).then(function(response){
                $scope.loan_request=response;
            },function(err){
                console.log("===Error in getting complete user details=====");
            })
     }])

    /*=============Borrower=================*/
    .controller('SelfLoanRequestsListCtrl', ["$scope", "$rootScope", "$timeout","$state","adapter", function ($scope, $rootScope, $timeout,$state,adapter) {
        $scope.loan_requests=[];
        $scope.createRequest=function(){
            $state.go("add_loan_request");
        }
        var request = {
            command: "getUserLoanRequests",
            collectionName: "iw.loan_requests",
            query:{
                q:{_id: $rootScope.iw.user._id }
            },
            method: 'get'
        }
        adapter.call(request).then(function (response) {
            console.log("=========Error in getting loan requests============")

            $scope.loan_requests=response.data;
        },function (response) {
            console.log("=========Error in getting loan requests============")
        });
    }])
    .controller('LoanRequestsAddCtrl', ["$scope", "$rootScope", "$timeout","$state","adapter",
        function ($scope, $rootScope, $timeout,$state,adapter) {
            $scope.fields={};
            $scope.submit=function() {
                var request = {
                    command: "addLoanRequest",
                    collectionName: "iw.loan_requests",
                    method: 'post',
                    data: $scope.fields
                }
                request["data"]["user_id"]=$rootScope.iw.user._id;
                adapter.call(request).then(function (response) {
                        $state.go("self_requests_list");
                    },
                    function (response) {
                        console.log('===================Error in adding loan request===============', response)
                    });
            }
        }])

    /*================Listing================*/
    .controller('LoanOffersListCtrl', ["$scope", "$rootScope", "$timeout","$state","$compile","$http","adapter","modalTemplate",
        function ($scope, $rootScope, $timeout,$state,$compile,$http,adapter,modalTemplate) {
            var selected_offer=null;
            var filters={};
            $scope.request_id="";
            $scope.loan_offers=[];
            $scope.loan_requests=[];
            $scope.selected_request={};

            var getUserLoanRequests=function(cb){
                var request = {
                    command: "getUserLoanRequests",
                    collectionName: "iw.loan_requests",
                    query:{
                        q:{_id: $rootScope.iw.user._id }
                    },
                    method: 'get'
                }
                adapter.call(request).then(function (response) {
                    cb(null,response.data);
                },function (response) {
                    cb(response.data,null);
                    console.log("=========Error in getting loan requests============")
                });
            }

            var getData=function(){
                var temp=[];
                angular.forEach(filters,function(val,key){
                    temp.push(val);
                })
                var request = {
                    command: "searchLoanOffers",
                    collectionName: "iw.loan_offers",
                    query:{
                        q:{},
                        filters:temp
                    },
                    method: 'get'
                }
                adapter.call(request).then(function (response) {
                    $scope.loan_offers=response.records;
                },function (response) {
                    console.log("=========Error in getting loan requests============")
                });
            }

            $scope.sendReceiveBidRequest=function(){
                var request = {
                    command: "requestToBid",
                    collectionName: "iw.mappings",
                    method: 'post',
                    data: {
                        request_id:$scope.request_id,
                        offer_id :selected_offer._id
                    }
                }
                adapter.call(request).then(function (response) {
                        alert("Bid Request placed successfully");
                        modalTemplate.removeModal();
                    },
                    function (response) {
                        modalTemplate.removeModal();
                    });
            }

            $scope.receiveRequestForBid=function(offer) {
                if(!$rootScope.iw.user.isLoggedIn){
                    return $state.go("login");
                }
                if($rootScope.iw.user.role!="borrower"){
                    return alert("As a lender you can't send Bidding request");
                }
                getUserLoanRequests(function(err,response){
                    if(err){
                        return console.log("-------------err-------------",err);
                    }
                    if(!response.length){
                        return alert("You have not created any loan request before requesting for loan. Please create it first");
                    }
                    selected_offer=offer;
                    $scope.request_id=response[0]._id;
                    if(response.length==1){
                        $scope.sendReceiveBidRequest();
                    }
                    else{
                        $scope.loan_requests=response;
                        modalTemplate.appendTemplate("select_loan_request",$scope)
                    }
                })

            }

            $scope.setFilters=function(name,value){
                if(!name){
                    return ;
                }
                switch(name){
                    case "interest_rate_min":
                        filters["interest_rate_min"]={field:'interest_rate_min',value:value,operator:"greater_than_or_equal"}
                        break;
                    case "interest_rate_max":
                        filters["interest_rate_max"]={field:'interest_rate_max',value:value,operator:"less_than_or_equal"}
                        break;

                    case"duration":
                        if(!filters["duration"]){
                            filters["duration"]={field:'duration',value:[],operator:"in"}
                        }
                        if(filters["duration"]["value"].indexOf(value)>-1){
                            filters["duration"]["value"].splice(filters["duration"]["value"].indexOf(value),1)
                        }
                        else{
                            filters["duration"]["value"].push(value)
                        }
                        break;
                }
                getData();
            }

            if($state.params.city){
                filters["city"]={field:'city',value:[$state.params.city],operator:"in"}
            }
            if($state.params.max_interest){
                filters["interest_rate_max"]={field:'interest_rate_max',value:$state.params.min_interest,operator:"less_than_or_equal"}
            }
            if($state.params.purpose){
                filters["purpose"]={field:'purpose',value:[$state.params.purpose],operator:"in"};
            }
            getData();
        }])
    .controller('LoanRequestsListCtrl', ["$scope", "$rootScope", "$timeout","$state","$http","$compile","adapter","modalTemplate",
        function ($scope, $rootScope, $timeout,$state,$http,$compile,adapter,modalTemplate) {
            var selected_bid=null;
            var filters={};
            $scope.amount=0;
            $scope.max_amount=0;
            $scope.errors={};
            $scope.loan_requests=[];

            $scope.onStart = function() {
                console.info('started', $scope.slider_data.value);
            };
            $scope.onChange = function() {
                console.info('changed', $scope.slider_data.value);
                $scope.otherData.value = $scope.slider_data.value * 10;
            };
            $scope.onEnd = function() {
                console.info('ended', $scope.slider_data.value);
            };
            var getData=function(){
                var temp=[];
                angular.forEach(filters,function(val,key){
                    temp.push(val);
                })
                var request = {
                    command: "searchLoanRequests",
                    collectionName: "iw.loan_requests",
                    query:{
                        q:{},
                        filters:temp
                    },
                    method: 'get'
                }
                adapter.call(request).then(function (response) {
                    $scope.loan_requests=response.records;
                },function (response) {
                    console.log("=========Error in getting loan requests============")
                });
            }

            var getMaxAmount=function(req,cb){
                var request = {
                    command: "getMaxAmount",
                    collectionName: "iw.mappings",
                    query:{
                        q:{_id: $rootScope.iw.user._id,request_id:req._id}
                    },
                    method: 'get'
                }
                adapter.call(request).then(function (response) {
                    cb(null,response.data);
                },function (response) {
                    cb(response.data,null);
                    console.log("=========Error in getting loan requests============")
                });
            }

            $scope.sendBiddingRequest=function(){
                $scope.errors={};
                if(!$scope.amount) {
                    return  $scope.errors["required"]=true;
                }
                if(isNaN($scope.amount)) {
                    return  $scope.errors["numeric"]=true;
                }
                if(parseInt($scope.amount)>$scope.max_amount) {
                    return  $scope.errors["max"]=true;
                }
                var request = {
                    command: "placeBid",
                    collectionName: "iw.mappings",
                    query:{
                        q:{_id: selected_bid._id}
                    },
                    data:{
                        request_id:selected_bid._id,
                        lender_id:$rootScope.iw.user._id,
                        amount:$scope.amount
                    },
                    method: 'post'
                }
                adapter.call(request).then(function (response) {
                    $scope.errors={};
                    $scope.amount=0;
                    $scope.max_amount=0;
                    selected_bid=null;
                    $state.go($state.current, {}, {reload: true});
                },function (response) {
                    $scope.errors={};
                    selected_bid=null;
                    $scope.amount=0;
                    $scope.max_amount=0;
                    console.log("=========Error in getting loan requests============")
                });
            }

            $scope.placeBid=function(req) {
                if(!$rootScope.iw.user.isLoggedIn){
                    return $state.go("login");
                }
                if($rootScope.iw.user.role!="lender"){
                    return alert("As a borrower you can't Bid");
                }
                selected_bid =req;
                var request = {
                    command: "getMaxAmount",
                    collectionName: "iw.mappings",
                    query:{
                        q:{request_id: req.request_id,offer_id:req.offer_id}
                    },
                    method: 'get'
                }
                adapter.call(request).then(function (response) {
                    $scope.amount=parseInt(response.amount);
                    $scope.max_amount=$scope.amount;
                    modalTemplate.appendTemplate("select_amount_for_mapping",$scope)

                },function (response) {
                    console.log("=========Error in getting max amount============")
                });
            }

            $scope.setFilters=function(name,value){
                if(!name){
                    return ;
                }
                switch(name){
                    case "interest_rate_min":
                        filters["interest_rate_min"]={field:'interest_rate_min',value:value,operator:"greater_than_or_equal"}
                        break;
                    case "interest_rate_max":
                        filters["interest_rate_max"]={field:'interest_rate_max',value:value,operator:"less_than_or_equal"}
                        break;
                    case"city":
                        if(!filters["city"]){
                            filters["city"]={field:'city',value:[],operator:"in"}
                        }
                        if(filters["city"]["value"].indexOf(value)>-1){
                            filters["city"]["value"].splice(filters["city"]["value"].indexOf(value),1)
                        }
                        else{
                            filters["city"]["value"].push(value)
                        }
                        break;
                    case"duration":
                        if(!filters["duration"]){
                            filters["duration"]={field:'duration',value:[],operator:"in"}
                        }
                        if(filters["duration"]["value"].indexOf(value)>-1){
                            filters["duration"]["value"].splice(filters["duration"]["value"].indexOf(value),1)
                        }
                        else{
                            filters["duration"]["value"].push(value)
                        }
                        break;
                    case"risk_rating":
                        if(!filters["risk_rating"]){
                            filters["risk_rating"]={field:'risk_rating',value:[],operator:"in"}
                        }
                        if(filters["risk_rating"]["value"].indexOf(value)>-1){
                            filters["risk_rating"]["value"].splice(filters["risk_rating"]["value"].indexOf(value),1)
                        }
                        else{
                            filters["risk_rating"]["value"].push(value)
                        }
                        break;
                }
                getData();
            }

            if($state.params.city){
                filters["city"]={field:'city',value:[$state.params.city],operator:"in"}
            }
            if($state.params.min_interest){
                filters["interest_rate_min"]={field:'interest_rate_min',value:$state.params.min_interest,operator:"greater_than_or_equal"}
            }
            if($state.params.purpose){
                filters["purpose"]={field:'purpose',value:[$state.params.purpose],operator:"in"};
            }
            getData();

        }])
