/**
 * Created with JetBrains WebStorm.
 * User: root
 * Date: 2/10/15
 * Time: 9:02 AM
 * To change this template use File | Settings | File Templates.
 */
/**
 * Created with JetBrains WebStorm.
 * User: root
 * Date: 27/9/15
 * Time: 8:49 AM
 * To change this template use File | Settings | File Templates.
 */


var req = require("request");
var path = require("path");
var fs = require("fs");

var location="../app/geo_data";
var api_url="https://www.WhizAPI.com/api/v2/util/ui/in";
var api_key="fs3tindy28wjop664fca1sc4";
req.get(api_url+"/indian-states-list?appkey="+api_key, function (err, ress, body) {
    if (typeof ress.body == 'string') {
        ress.body = JSON.parse(ress.body)
    }
    var data = [];
    ress.body.Data.forEach(function (state) {
        var temp = {}
        temp["name"] = state["Name"];
        temp["value"] = state.Name.trim().replace(/ /g, "_").toLowerCase();
        temp["type"] = state["Type"];
        data.push(temp)
    })
    var cities = {}
    var fetchCities = function (states, next) {
        if (!states.length) {
            return next();
        }
        var state = states.shift();
        var name = state.Name.trim().replace(/ /g, "_").toLowerCase();
        req.get(api_url+"/indian-city-by-state?stateid=" + state.ID + "&appkey="+api_key, function (err, ress, body) {
            if (typeof ress.body == 'string') {
                ress.body = JSON.parse(ress.body)
            }
            var data = [];
            ress.body.Data.forEach(function (state) {
                var temp = {}
                temp["value"] = state["city"];
                temp["name"] = state.city.trim().replace(/ /g, "_").toLowerCase();
                data.push(temp);
            })
            cities[name] = data;
            console.log('====================', name)
            fetchCities(states, next);
        })
    }

    fs.writeFile(path.join(__dirname, location, '/states.json'), JSON.stringify({states: data}), function (err) {
        if (err) throw err;
        console.log('It\'s saved!');
        fetchCities(ress.body.Data, function () {
            fs.writeFile(path.join(__dirname, location, '/cities.json'), JSON.stringify({cities: cities}), function (err) {
                if (err) throw err;
                console.log('It\'s saved!');
            });
            console.log("======done============")
        })
    });
})


