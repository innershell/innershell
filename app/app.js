/**
 * Created with JetBrains WebStorm.
 * User: root
 * Date: 21/8/15
 * Time: 5:32 PM
 * To change this template use File | Settings | File Templates.
 */
var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var bodyParser = require('body-parser');
var debug = require('debug')('untitled:server');
var http = require('http');
var nconf=require("nconf");
var busboy = require('connect-busboy'); //middleware for form/file upload
var assetManager = require('connect-assetmanager');

var app = express();
nconf.argv().env();

require('iw-config').loadConfigs();  //load env config

//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));

app.use(require("iw-session").getSession());

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(busboy({
    highWaterMark: 2 * 1024 * 1024,
    limits: {
        fileSize: 10 * 1024 * 1024
    }
}));

app.use(function(req,res,next){
    console.log("req.url ",req.url)
    if(!req.session.user) {
        req.session.user={
            isLoggedIn:false
        }
    }
    next();
})
app.use('/admin',function(req,res,next){
    // for permission check in case of admin
    next()
});


var assetManagerGroups = {
    'js': {
        'route': /\/js\/framework\/investors_wallet.min.js/, 'path': __dirname + '/public/js/framework/', 'dataType': 'javascript', 'files': [
             'validation.js'
            , 'capcha.js'
            , 'range-filter.js'
            , 'config.js',
            , 'services.js',
            , 'controllers.js'
            , 'directives.js'
            , 'filters.js'
            , 'app.js'
        ]
    },
    'css': {
        'route': /\/css\/investors_wallet.min.css/, 'path': __dirname + '/public/css/' , 'dataType': 'css', 'files': [
             'bootstrap.min.css'
            ,'font-awesome.min.css'
            ,'style.css'
        ]
    }
}
app.use(assetManager(assetManagerGroups));

app.use(express.static(__dirname + '/public'));


app.use(function(req,res,next){
    require("iw-db").getDb(function(db){
        if(!db){
            res.status(500);
            res.send("Connection to db failed");
        }
        req.db=db;
        next()
    });
})

app.use(function(req,res,next){
    require("iw-app").createAppFromRequest(req,res,next);
})

require("./routes/collections")(app);
require("./routes/payment")(app);
require("./routes/index")(app);

app.use(function(req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
});

process.on('uncaughtException', function (err) {
    console.error('here in error=========================>>>>>>>>>>', err);
    console.error('error', err.stack);
});

if (nconf.get('env') === 'production') {
    app.use(function(err, req, res, next) {
        res.status(err.status || 404);
        res.send();
    });
}

if (nconf.get('env') === 'development') {
    app.use(function(err, req, res, next) {
        throw err;
    });
}

module.exports = app;






