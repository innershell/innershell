/**
 * Created with JetBrains WebStorm.
 * User: root
 * Date: 3/10/15
 * Time: 9:49 AM
 * To change this template use File | Settings | File Templates.
 */

var checkAjaxValidity=function(req,res,next){
    if(!req.headers.ajax){
        res.status(401);
        return res.send("Unauthorized Access of API");
    }
    next();
}

var parseInformation=function(req,res,next){
    if(req.query && req.query.q && typeof req.query.q=='string'){
        req.query.q=JSON.parse(req.query.q)
    }
    if(req.query && req.query.filters && typeof req.query.filters=='string'){
        req.query.filters=JSON.parse(req.query.filters)
    }
    next();
}

var handleCustomRequests=function(req,res,next){
    var request={
        command:req.params.command,
        collectionName:req.params.collection,
        collectionId:req.params.collectionId,
        data:req.body,
        query:req.query,
        files:res.files
    }
    req.myApp.call(request,function(err,response){
        if(err){
            res.status(err.status)
            return res.send(err.data)
        }
        res.status(response.status)
        res.send(response.data)
    })
}

var handleListCommand=function(req,res,next){
    var request={
        collectionName:req.params.collection,
        data:req.body,
        query:req.query,
        files:res.files
    }
    req.myApp.list(request,function(err,response){
        if(err){
            res.status(err.status);
            return res.send(err.data);
        }
        res.status(response.status);
        res.send(response.data);
    })
}



module.exports=function(parent){
    /*parent.post("/testing",function(req,res,next){
        var multiparty = require('multiparty');
        var form = new multiparty.Form();

        form.parse(req, function(err, fields, files) {
            console.log('==========================',req.file,req.body,fields);

        });

    });*/
    parent.all("/collection/:collection/_/:command",checkAjaxValidity,parseInformation,handleCustomRequests);
    parent.get("/collection/:collection",checkAjaxValidity,parseInformation,handleListCommand);
}