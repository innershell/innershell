var express = require('express');
var router = express.Router();


var checkAjaxValidity=function(req,res,next){
   if(req.headers.ajax){
       res.status(404);
       return res.send();
   }
    next();
}

var sendSiteMap=function(req,res,next){
    res.status(200);
    res.sendFile(require("path").join(__dirname,"../public/default.html"));
}

var sendUserInfo=function(req,res,next){
    res.type('application/javascript');
    var output = "session = " + JSON.stringify(req.session.user);
    res.send(output);
}

var sendDefaultLayout=function(req,res,next){
    // sending 404 for static file request
    if( /^.*\.(jpg|jpeg|gif|png|css|js|ico|xml|rss|txt)$/.test(req.url)){
        res.status(404);
        return res.send();
    }
    res.status(200);
    res.sendFile(require("path").join(__dirname,"../public/default.html"));
}

var getCommonConfig=function(req,res,next){
    require("iw-config").loadCommonConfigs(function(err,data){
        if(err){
            res.status(406);
            return res.send(err);
        }
        res.status(200)
        res.type('application/javascript');
        var output = "config = " + JSON.stringify(data);
        res.send(output);
    })
}


module.exports=function(parent){
    parent.get("/userInfo.js",checkAjaxValidity,sendUserInfo);
    parent.get("/clientConfig.js",checkAjaxValidity,getCommonConfig);
    parent.get("/sitemap.xml",checkAjaxValidity,sendSiteMap);
    parent.get("*",checkAjaxValidity,sendDefaultLayout);
}
