/**
 * Created with JetBrains WebStorm.
 * User: root
 * Date: 2/10/15
 * Time: 8:38 AM
 * To change this template use File | Settings | File Templates.
 */


module.exports=function(parent){
    parent.post("/handleResponse/:status/:txn_id",function(req,res,next){
        var request={
            collection:"iw.users",
            data:req.body,
            command:"handlePaymentResponse"
        }
        req.myApp.call(request,function(err,response){
            if(err){
                return res.sendFile(require("path").join(__dirname,"../public/payment_error.html"));
            }
            res.redirect("/account/dashboard");

        })
    });
}


