#!/usr/bin/env node
/**
 * Created with JetBrains WebStorm.
 * User: root
 * Date: 25/9/15
 * Time: 10:53 AM
 * To change this template use File | Settings | File Templates.
 */

var app = require('../app');
var debug = require('debug')('test:server');
var http = require('http');

/**
 * Get port from environment and store in Express.
 */

var port = normalizePort(process.env.APP_PORT);
app.set('port', port);

/**
 * Create HTTP server.
 */

var server = http.createServer(app);

/**
 * Listen on provided port, on all network interfaces.
 */
server.listen(port);
server.on('error', onError);
server.on('listening', onListening);

/**
 * Normalize a port into a number, string, or false.
 */

function normalizePort(val) {
    var port = parseInt(val, 10);

    if (isNaN(port)) {
        // named pipe
        return val;
    }

    if (port >= 0) {
        // port number
        return port;
    }

    return false;
}

/**
 * Event listener for HTTP server "error" event.
 */

function onError(error) {
    if (error.syscall !== 'listen') {
        throw error;
    }

    var bind = typeof port === 'string'
        ? 'Pipe ' + port
        : 'Port ' + port;

    // handle specific listen errors with friendly messages
    switch (error.code) {
        case 'EACCES':
            console.error(bind + ' requires elevated privileges');
            process.exit(1);
            break;
        case 'EADDRINUSE':
            console.error(bind + ' is already in use');
            process.exit(1);
            break;
        default:
            throw error;
    }
}

/**
 * Event listener for HTTP server "listening" event.
 */

function onListening() {
    var addr = server.address();
    var bind = typeof addr === 'string'
        ? 'pipe ' + addr
        : 'port ' + addr.port;
    debug('Listening on ' + bind);

    console.log("===============listening on============")
}


/*process.on('SIGTERM', function () {
 console.log("Received SIGTERM. Gracefully shutting down");
 process.exit(1);

 });
 process.on('SIGINT', function () {
 console.log("Received SIGINT. Gracefully shutting down");
 process.exit(1);

 });
 process.on('message', function (msg) {
 console.log("Received shutdown message. Gracefully shutting down");
 if (msg == 'shutdown') {
 process.exit(1);
 }
 });*/

